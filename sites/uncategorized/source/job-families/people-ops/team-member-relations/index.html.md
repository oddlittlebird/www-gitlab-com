---
layout: job_family_page
title: "Team Member Relations"
---

## Levels

### Team Member Relations Specialist

The Team Member Relations Specialist reports to Director, People Ops.

#### Team Member Relations Specialist Job Grade

The Team Member Relations Partner is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Team Member Relations Specialist Responsibilities

* Responds to team members and leaders request for guidance and policy on a broad range of team member relation matters.  
* Partners with People Business Partners , other teams and People leaders to ensure that Investigations/* Disciplinary processes are handled in a fair, timely manner consistent with local requirements including all documentation.
* Provides Team Member Relations support, thought partnership, and coaching for all team members and levels of management in the organization on issues including absenteeism, performance, conduct and harassment.
* Partners with Legal and People Compliance to ensure compliance with policies, practices and applicable employment legislation.
* Balance a broad range of tasks including the interpretation and development of policies and procedures, facilitating training on key Team Member Relations issues, and contributing to the development and implementation of Team Member Relations programs based on line of business goals and People Group strategy.
* Identifies process improvement opportunities and policy gaps. 

#### Team Member Relations Specialist Requirements

* Effective communication and problem solving skills.
* Must exercise confidentiality and discretion in dealing with sensitive, complex and time sensitive employment matters
* Demonstrates empathy and experience driving inclusive culture.
* Minimum of 2 year global experience managing team member relation cases.
* Demonstrated solid judgement and experience assisting risk relative to the business.
* Experience learning and thriving in a constantly changing Global environment and to cultivate relationships across Global teams.
* Be a trusted adviser by providing consultation and resolution guidance to promote a positive GitLab culture.
* Ability to use GitLab
* Aligns with GitLabs values

#### Team Member Relations Partner

The Team Member Relations Partner reports to Director, People Ops.

#### Team Member Relations Partner Job Grade

The Team Member Relations Partner is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Team Member Relations Partner Responsibilities

* Extends that of the Team Member Specialist responsibilities
* Manage sensitive Team Member relations issues and conduct confidential investigations in partnership with  Legal and People Business Partners.
* Investigates highly sensitive People issues, including: conducting claimant, witness and accused team member interviews; analyzing data from various internal systems; composing investigation reports; proposing remedial action based on investigation findings. 
* Coaching of managers through delicate, complex and often confidential work issues.
* Responsible for owning team members or candidate requests for workplace accommodations. 
* Creates proactive programs designed to prevent workplace issues/concern

#### Team Member Relations Partner Requirements

* Extends that of the Team Member Specialist requirements
* 5+ years’ experience in Employee Relations.
* Solid understanding  of federal, state or local labor laws.
* Strong collaboration skills and ability to take direction effectively.
* Ability to effectively manage and prioritize multiple tasks.
* Strong project management, change management, and experience driving programs independently.
* Handles confidential information with care and utmost discretion.

### Senior Team Member Relations Partner

The Senior Team Member Relations Partner reports to Director, People Ops.

#### Senior Team Member Relations Partner Job Grade

The Senior Team Member Relations Partner is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Team Member Relations Partner Responsibilities

* Extends that of the Team Member Relations Partner responsibilities
* Investigates complex team member concerns that could have significant legal, regulatory, brand,  financial or other risks to GitLab and provide appropriate course of action depending on outcome of investigations.
* Partner with Legal and PBP on the review and assessment of
* Provides critical input to the formation, consistent application, and revision of policies across GitLab as we continue to scale.
* Provide trend data and actionable feedback to legal, HRBP teams, and senior leaders to reduce overall risk in the organization. 
* Regularly reviews policies, procedures and initiative to ensure they align with applicable regulatory requirements as well as GitLabs’ Values.  

#### Senior Team Member Relations Partner Requirements

* Extends that of the Team Member Relations Partner requirements
* Degree in Human Resources, Employee Relations, or Employment Law.
* 8+ years of progressive investigatory or employee relations experience.
* Excellent written and verbal communication and interpersonal skills.
* Strong problem-solving skills, critical thinking, and intuitive sense of business acumen.
* Ability to execute tasks with attention to detail, reliability, and a relatively quick pace.
* Ability to interface and partner with People and non-People stakeholders to achieve results

### Manager, Team Member Relations

The Manager, Team Member Relations reports to Director, People Ops.

#### Manager, Team Member Relations Job Grade

The Manager, Team Member Relations is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Team Member Relations Responsibilities

* Extends that of the Senior Team Member Relations Partner responsibilities
* Manages a team of team member relation specialists and managers.
* Acts as a subject matter expert to provide strategic coaching to management of all levels on addressing a variety of people issues while effectively mitigating business and company risk.
* Lead department specific and org wide strategic initiatives, soliciting buy in, partnering with cross-functional partners and driving scalable solutions.
* Establish strategic partnerships and support business units on employee relations concerns, performance management initiatives and change management.
* Ensure oversight and governance of policies and practices globally, including interpreting and applying understanding of corporate policies and practices, employment laws, and other regulations to provide advice, guidance, or clarification for Team Member Relations inquiries.
* Analyze data to identify trends, provide insights and make recommendations to stakeholders and cross-functional partners to develop scalable solutions, programs and policies to improve the team member experience.

#### Manager, Team Member Relations Requirements

* Extends that of the Senior Team Member Relations Partner requirements
* 8+ years of employee relations experience
* 2+ years of managing a team 
* Comfortable adapting to change in a fast-paced, global environment
* Exceptional interpersonal and communication skills
* Strong analytical skills and experience Google Sheets
* Ability to prioritize and take ownership of projects

## Performance Indicators

* Team member Case Management
   * Team member relations team will respond to all requests for assistance from team members within 24 hours. 
   * All cases are resolved in a consistent, fair and unbiased process.  
   * Identify potential risks to GitLab and escalate to Key Stakeholders for mitigation.
* Team member Relation Philosophy
   * All team members are treated with respect and dignity while upholding the values of GitLab. 
Provide all GitLab team members an avenue to express workplace concerns and to resolve conflicts in a safe and unbiased forum.
* Collaboration
   * Effective partnership with key stakeholders such as People Business Partners, Legal and People Success.  

## Career Ladder

The next step in the People Operations job family is not yet defined.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with our Senior Manager, People Operations
* After that, candidates will be invited to schedule a 30 minute interview with members of the People Business Partner Team
* Next, candidates will be invited to schedule a 30 minute interview with members of the Legal team
* After that, candidates will be invited to interview with the Senior Director, People Success
* Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
